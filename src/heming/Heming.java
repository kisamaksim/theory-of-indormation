package heming;

import java.util.Arrays;
import java.util.Scanner;

public class Heming {

    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int FOUR = 4;
    private static final int EIGHT = 8;
    private static final int SIXTEEN = 16;

    private static int[] bits = new int[6];


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] word = new int[26];
        String temp = scanner.nextLine();
        char[] temp2 = temp.toCharArray();

        for (int i = 0; i < temp2.length; i++) {
            word[i + 1] = Integer.parseInt(String.valueOf(temp2[i]));
        }

        word = addBits(word);
        System.out.println("Message after add bits: ");
        for (int i = 1; i < word.length; i++) {
            System.out.print(word[i]);
        }

        System.out.println();
        System.out.println("-----------------------");
        word = countBits(word);
        System.out.println("Message after count bits: ");
        for (int i = 1; i < word.length; i++) {
            System.out.print(word[i]);
        }
        System.out.println();
        System.out.println("Counted bits: ");
        for (int i = 1; i < bits.length; i++) {
            System.out.print(bits[i]);
        }
        System.out.println();
        System.out.println("-----------------------");

        System.out.println("Enter number of ill bit: ");
        int mistake = scanner.nextInt();
        word = makeMistake(word, mistake);
        System.out.println("Message after add mistake: ");
        for (int i = 1; i < word.length; i++) {
            System.out.print(word[i]);
        }
        System.out.println();
        System.out.println("----------------------");

        int illbit = decoding(word);







    }

    public static int[] addBits(int[] word) {

        int last = 21;

        for (int i = last - 1; i >= ONE; i--) {
            word[i + 1] = word[i];
        }
        word[ONE] = 0;
        last++;

        for (int i = last - 1; i >= TWO; i--) {
            word[i + 1] = word[i];
        }
        word[TWO] = 0;
        last++;

        for (int i = last - 1; i >= FOUR; i--) {
            word[i + 1] = word[i];
        }
        word[FOUR] = 0;
        last++;

        for (int i = last - 1; i >= EIGHT; i--) {
            word[i + 1] = word[i];
        }
        word[EIGHT] = 0;
        last++;

        for (int i = last - 1; i >= SIXTEEN; i--) {
            word[i + 1] = word[i];
        }
        word[SIXTEEN] = 0;
        last++;

        return word;
    }

    public static int[] countBits(int[] word) {

        int[] helparr = {0, ONE, TWO, FOUR, EIGHT, SIXTEEN};

        for (int i = 1; i < helparr.length; i++) {

            int counter = 0;

            for (int j = helparr[i]; j < word.length; j += helparr[i]) {

                int c = 0;
                int helpJ = j;
                while (c < helparr[i]) {

                    counter += word[helpJ];
                    if (helpJ == (word.length -1)) {
                        break;
                    }
                    helpJ++;
                    c++;
                }
                j = helpJ;
            }
            if ((counter % 2) != 0) {
                word[helparr[i]] = 1;
                bits[i] = 1;
            }
        }

        return word;
    }

    public static int[] makeMistake (int[] word, int index) {

        if (word[index] == 1) {
            word[index] = 0;
        } else {
            word[index] = 1;
        }

        return word;
    }

    public static int decoding (int[] word) {

        int[] helparr = {0, ONE, TWO, FOUR, EIGHT, SIXTEEN};
        int[] decodingBits = new int[6];

        for (int i = 1; i < helparr.length; i++) {

            int counter = 0;

            for (int j = helparr[i]; j < word.length; j += helparr[i]) {

                int c = 0;
                int helpJ = j;
                while (c < helparr[i]) {

                    counter += word[helpJ];
                    if (helpJ == (word.length -1)) {
                        break;
                    }
                    helpJ++;
                    c++;
                }
                j = helpJ;
            }
            counter -= word[helparr[i]];
            if ((counter % 2) != 0) {
                decodingBits[i] = 1;
            }
        }

        System.out.println("Sindrome: ");
        for ( int i = 1; i < decodingBits.length; i++) {
            System.out.print(decodingBits[i]);
        }

        System.out.println();
        int illBit = 0;
        for (int i = 1; i < decodingBits.length; i++) {

            if (decodingBits[i] != word[helparr[i]]) {
                illBit += helparr[i];
            }
        }

        if (illBit == 0) {
            System.out.println("Message is right, not problem");
            return 0;
        } else {
            System.out.println("Bit is broken, ill bit is: " + illBit);
            return illBit;
        }
    }

    public static int[] solveProblem(int[] word, int index) {

        if (word[index] == 1) {
            word[index] = 0;
        } else {
            word[index] = 1;
        }
        return word;
    }
}
